<?php print '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>'; ?>
<?php if(!$variables['label_hidden']) : ?>
  <?php print'<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>'; ?>
<?php endif; ?>
<?php print '<div class="field-items"' . $variables['content_attributes'] . '>'; ?>
<?php
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    print '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
    $media_embed_config = media_embed_fetch_config($item['file']['#file']->fid);
    if (isset($media_embed_config->show_embed) && ($media_embed_config->show_embed)) {
      if ($item['file']['#theme'] == 'file_link') {
        $embed_code = theme('media_embed_generate', array('fpath' => $item['file']['#file']->uri, 'media_embed_config' => $media_embed_config));
      }
      elseif (($item['file']['#theme'] == 'image') || ($item['file']['#theme'] == 'image_style')) {
        $embed_code = theme('media_embed_generate', array('fpath' => $item["file"]["#path"], 'media_embed_config' => $media_embed_config));
      }
      if ($media_embed_config->link_text) {
        $link_text = $media_embed_config->link_text;
      }
      else {
        $link_text = "View Embed Code";
      }
      print theme('media_embed_container', array('embed_code' => $embed_code, 'fid' => $item['file']['#file']->fid, 'link_text' => $link_text));
    }
  }
  print '</div>';
?>
</div>