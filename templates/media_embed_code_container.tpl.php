<?php
  if ($embed_code && $fid) {
    print '<br>';
    print '<a href="javascript:void(0);" class="show-embed-code" id="embed-code-container_' . $fid . '" copy_id= "media-embed-code_' . $fid . '" container_id = "embed-code-container_' . $fid . '">' . $link_text .'</a>';
    print '<br>';
    print '<div class = "embed-code-container_' . $fid . ' embed-code-container">';
    print '<label for="media-embed-code">Embed Code:</label>';
    print '<textarea class="media-embed-code_' . $fid . ' embed-code" cols="80" rows="2" suffix="'. $fid. '" disabled>' . $embed_code . '</textarea><br>';
    print '</div>';
  }
?>