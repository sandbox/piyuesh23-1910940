(function ($) {
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.media_embed_toggle = { attach: function (context, settings) {
    $('a.show-embed-code').click(function(){
      var textareaId = $(this).attr('copy_id');
      var containerId = $(this).attr('container_id');
      $('.'+containerId).dialog({"modal":true, "draggable":false, "width":560, "title":'Embed Code', "resizable":false}); 
      $('.'+textareaId).dialog('open');
      return false;
    });
  }};
}(jQuery));
